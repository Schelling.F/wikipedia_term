# Wikipedia Term

Command Line tools for wikipedia written in python: <br>
**Author** Felix Schelling <br>
**Date** 16.01.2021  <br>
**License:** MIT <br>
***

**Options:**
  - -h, --help            show this help message and exit
  - -s SEARCH, --search=SEARCH
                        specify an search query if empty or not providen
                        random article is choosen
  - -p, --pdf             renders wikipage to pdf and opens pdf viewer
  - -f, --full            show initre content
  - -d, --deutsch         search and show in german

**Dependcies:**
- Python 3
- zatuha 
- pip:
  - wikipedia
  - pdfkit